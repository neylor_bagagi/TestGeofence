package com.cyanu.testgeofence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by neylorbagagi on 16/10/16.
 */

public class GeofenceBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_GEOFENCE_TRIGGER = "com.cyanu.testgeofence.action.GEOFENCE_TRIGGER";

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("BroadcastReceiver RESULT"+intent.getAction());
        System.out.println("BroadcastReceiver EXTRA"+intent.getStringExtra("geofence"));
    }

}
