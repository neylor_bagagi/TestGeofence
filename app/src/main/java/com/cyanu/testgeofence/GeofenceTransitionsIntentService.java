package com.cyanu.testgeofence;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by neylorbagagi on 15/10/16.
 */
public class GeofenceTransitionsIntentService extends IntentService {

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        System.out.println("######################");
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,geofencingEvent.getErrorCode());
            System.out.println("ERROR MESSAGE : "+errorMessage);
            return;
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(GeofenceBroadcastReceiver.ACTION_GEOFENCE_TRIGGER);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);


        switch(geofencingEvent.getGeofenceTransition()) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                System.out.println("ENTERED ON GEOFENCE");
                System.out.println("ENTERED ON GEOFENCE : "+geofencingEvent.getTriggeringLocation().toString());

                List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
                List<String> geofencesRequestId =  new ArrayList<String>();
                for (Geofence geofence : geofences) {
                    geofencesRequestId.add(geofence.getRequestId());
                }

                broadcastIntent.putStringArrayListExtra("geofences", (ArrayList<String>) geofencesRequestId);
                getApplicationContext().sendBroadcast(broadcastIntent);

                sendNotification("PET ON "+geofencesRequestId.get(0));


                break;
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                System.out.println("DWELLING ON GEOFENCE");
                break;
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                System.out.println("EXITED ON GEOFENCE");
                break;
            default:
                System.out.println("SOME ANOTHER GEOFENCE ?????");
        }
    }

    /**
     * Posts a notification in the notification bar when a transition is detected.
     * If the user clicks the notification, control goes to the MainActivity.
     */
    private void sendNotification(String notificationDetails) {
        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        // Define the notification settings.
        builder.setSmallIcon(R.drawable.ic_launcher)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher))
                .setColor(Color.YELLOW)
                .setContentTitle(notificationDetails)
                .setContentText(getString(R.string.geofence_transition_notification_text))
                .setContentIntent(notificationPendingIntent);

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);


        // Issue the notification
        mNotificationManager.notify(0, builder.build());
    }
}
