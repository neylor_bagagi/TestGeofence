package com.cyanu.testgeofence;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
                                                               GoogleApiClient.OnConnectionFailedListener,
                                                               com.google.android.gms.location.LocationListener,
                                                               ResultCallback<Status>{

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 0;
    private GoogleApiClient mGoogleApiClient;
    private TextView textViewLat;
    private TextView textViewLon;
    private ListView listViewGeofences;
    private Location mLastLocation;
    private LocationRequest locationRequest;
    private List<Geofence> mGeofenceList;
    private GeofencingRequest mGeofencingRequest;
    private PendingIntent mGeofencePendingIntent;
    private BroadcastReceiver geofenceBroadcastReceiver;
    private ArrayAdapter<String> geofencesAdapter;
    private ArrayList<String> receiverGeofencesList;

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            getUserLocationUpdates();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        textViewLat = (TextView)findViewById(R.id.textViewLat);
        textViewLon = (TextView)findViewById(R.id.textViewLon);
        listViewGeofences = (ListView)findViewById(R.id.listViewGeofences);

        receiverGeofencesList = new ArrayList<String>();
        geofencesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, receiverGeofencesList);
        listViewGeofences.setAdapter(geofencesAdapter);



        // START GEOFENCE
        // CREATE GEOFENCE LIST
        mGeofenceList = getGeofenseList();

        mGeofencingRequest = getGeofencingRequest(mGeofenceList);

        // SEND TO SERVICE
        mGeofencePendingIntent = getGeofencePendingIntent(mGeofencePendingIntent);

        // REGISTER BROADCASTRECEIVER AND THEIR FILTER INTENT
        IntentFilter filter = new IntentFilter(GeofenceBroadcastReceiver.ACTION_GEOFENCE_TRIGGER);
        filter.addCategory(Intent.CATEGORY_DEFAULT);

        geofenceBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                populateGeofenceListView(intent);
            }
        };

        registerReceiver(geofenceBroadcastReceiver, filter);



    }


    /* -----------------------
           CLASS METHODS
    ----------------------- */
    private void populateGeofenceListView(Intent intent){
        ArrayList<? extends String> geofencesListExtra = intent.getParcelableArrayListExtra("geofences");

        for (String geofence : geofencesListExtra) {
            geofencesAdapter.add(geofence);
        }
    }


    /* -----------------------
         LOCATION METHODS
    ----------------------- */
    private void getUserLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            updateUserLocationInUI(mLastLocation);
        } else {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE, android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
    }

    private void getUserLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationRequest = new LocationRequest();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }else {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE, android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
    }

    private void updateUserLocationInUI(Location location){
        if (location != null) {
            textViewLat.setText(String.valueOf(location.getLatitude()));
            textViewLon.setText(String.valueOf(location.getLongitude()));
            System.out.println("LAT:"+location.getLatitude()+" LNG:"+location.getLongitude());
        }
    }

    private void putGeofencesOnStream(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(mGeofenceList),
                    getGeofencePendingIntent(mGeofencePendingIntent)
            ).setResultCallback(this);
        }else {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE, android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
    }




    /* -----------------------
         GEOFENCE METHODS
    ----------------------- */
    private List<Geofence> getGeofenseList(){
        List<Geofence> geofenceList = new ArrayList<>();
        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("CASA")
                .setCircularRegion(-23.533865, -47.465941, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Guimarães e Maciel Prime")
                .setCircularRegion(-23.526917, -47.465011, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Posto Extra Campolim")
                .setCircularRegion(-23.524873, -47.464785, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("HABIB'S")
                .setCircularRegion(-23.522218, -47.464454, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Objetivo Sorocaba")
                .setCircularRegion(-23.520598, -47.461698, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Runner Premium")
                .setCircularRegion(-23.520773, -47.463721, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Colégio Uirapuru")
                .setCircularRegion(-23.516394, -47.461595, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Smart Fit")
                .setCircularRegion(-23.513090, -47.455798, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("PARIS")
                .setCircularRegion(-23.524484, -47.455754, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("UFSCar")
                .setCircularRegion(-23.582080, -47.523999, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Tropical Greill")
                .setCircularRegion(-23.511978, -47.456136, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

        geofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId("Seiren do Brasil")
                .setCircularRegion(-23.468723, -47.451059, 300)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());


        return geofenceList;
    }

    private GeofencingRequest getGeofencingRequest(List<Geofence> geofenceList) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofenceList);

        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent(PendingIntent pendingIntent) {
        // Reuse the PendingIntent if we already have it.
        if (pendingIntent != null) {
            return pendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }




    /* -----------------------
       GOOGLE API CALLBACKS
    ----------------------- */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getUserLastLocation();
        getUserLocationUpdates();
        putGeofencesOnStream();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }




    /* --------------------------------
       GMS.LOCATION.LOCATIONLISTENER
    -------------------------------- */
    @Override
    public void onLocationChanged(Location location) {
        updateUserLocationInUI(location);
        //Toast.makeText(this, "Location Updated", Toast.LENGTH_SHORT).show();
    }




    /* ----------------------------------
       LOCATION MANAGER PROVIDER STATUS
    ---------------------------------- */
    private void getProveiderStatus(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // GPS_PROVIDER IS enabled...
        } else {
            // GPS_PROVIDER is NOT enabled...
            System.out.println(LocationManager.GPS_PROVIDER+" DISABLE");
        }

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // NETWORK_PROVIDER IS enabled...
        } else {
            // NETWORK_PROVIDER is NOT enabled...
            System.out.println(LocationManager.NETWORK_PROVIDER+" DISABLE");
        }
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // We are good for geofencing as both GPS and Network providers are enabled....
            System.out.println(LocationManager.GPS_PROVIDER+" | "+LocationManager.NETWORK_PROVIDER+" ENABLE");
        }
    }




    /* -----------------------
          RESULTCALLBACK
    ----------------------- */
    @Override
    public void onResult(@NonNull Status status) {
        if (status.getStatusCode() == 1000){
            getProveiderStatus();
        }else {
            System.out.println("RESULT SUCCESS : "+status.getStatusMessage()+status.getStatus());
        }
    }

}
